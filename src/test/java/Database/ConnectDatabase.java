package Database;

import org.openqa.selenium.WebDriver;

//import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDatabase {

    protected WebDriver driver;

    public  ConnectDatabase (WebDriver driver)
    {
        this.driver=driver;
    }

          public  static void main(String[] args){
            try{
                Class.forName("org.postgresql.Driver");
                String url ="jdbc:postgresql://10.130.32.132:5432/sympan_audience_service?ApplicationName=${DOCKERHOSTNAME}__FROM_sympan_workflow_TO_sympan_audience&targetServerType=master";
                String user = "sympan";
                String password = "sympan";


                Connection connection = DriverManager.getConnection(url,user,password );

                String query = "select * from audience_history";
                Statement stmt = connection.createStatement();
                stmt.execute(query);

                Statement statement = connection.createStatement();
                ResultSet execute = statement.executeQuery(query);

                int count = 0;
                System.out.println("Connected to the database ");
                System.out.println("-------------------------------------------------------------------");
                System.out.println("ROWS  ||ID ||      CREATE_TIME       ||USER_NAME||");
                System.out.println("-------------------------------------------------------------------");
               // System.out.println("");


                while (execute.next()){
                    count++;
                    // System.out.println(execute.getInt("id"));
                    // System.out.println(execute.getString("create_time"));
                    // System.out.println(execute.getString("user_name"));

                    String id = execute.getString("id");
                    String create_time = execute.getString("create_time");
                    String user_name = execute.getString("user_name");

                    System.out.println("Row"+ " " + count + " || "+ id +    " || "+ create_time +"||"+ user_name +"||" );
                }
            }catch(SQLException e) {
                System.out.println(e.getMessage());
            }catch(ClassNotFoundException e){
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }


