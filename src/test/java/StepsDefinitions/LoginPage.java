package StepsDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import Pages.Browsers;


public class LoginPage {
    WebDriver driver;
    Browsers chrome;
    private final By btn_external = By.xpath("/html/body/div/div/div[2]/div/div[2]/div/div[2]/a");
    private final By txt_username = By.xpath("/html/body/div/div/div[3]/div/div[2]/div/div/div[2]/div[1]/div[2]/input");
    private final By txt_password = By.xpath("/html/body/div/div/div[3]/div/div[2]/div/div/div[2]/div[2]/div[2]/input");
    private final By btn_submit = By.xpath("/html/body/div/div/div[3]/div/div[2]/div/div/div[3]/div/a");
    private final By btn_create_service= By.xpath("/html/body/div/div/div[3]/div/div/div/div[2]/div[1]/img[1]");
    private final By btn_select_test = By.xpath("/html/body/div/div/div[3]/div/div/div/div[2]/div/div/div/div/div/div[2]/div[2]/div/div[1]/form[2]/div/div[1]/div/div[1]");
    private final By txt_select_type= By.xpath("/html/body/div/div/div[3]/div/div/div/div[2]/div/div/div/div/div/div[2]/div[2]/div/div[1]/form[2]/div/div[1]/div/div[2]/input");

    @Given("^the user is on login page$")
    public void the_user_is_on_login_page() throws InterruptedException {
        chrome = new Browsers (driver);
        chrome.chrome();
        Thread.sleep(2000);
    }


    @When("^the user enters (.*) and (.*)$")
    public void the_user_enters_valid_credentials(String username,String password) throws InterruptedException {
        chrome.driver.findElement(btn_external).click();
        //username
        chrome.driver.findElement(txt_username).sendKeys(username);
        Thread.sleep(2000);
        chrome.driver.findElement(txt_password).sendKeys(password);
        Thread.sleep(2000);
        System.out.println("Entered username and password");
        System.out.println("test");
        System.out.println("test2");
        Thread.sleep(2000);
    }
    @And("^hits submit$")
    public void hits_submit()  throws InterruptedException{
        chrome.driver.findElement(btn_submit).click();
        Thread.sleep(2000);
        System.out.println("Clicked on submit");
    }
    @Then("^the user should be logged in successfully$")
    public void the_user_should_be_logged_in_successfully()  throws InterruptedException{
        String kostas="SMS Trivia";
        Actions actions=new Actions(chrome.driver);
        Thread.sleep(2000);
        chrome.driver.findElement(btn_create_service).click();
        Thread.sleep(2000);
        chrome.driver.findElement(btn_select_test).click();
        Thread.sleep(2000);
        chrome.driver.findElement(txt_select_type).sendKeys(kostas);
        Thread.sleep(2000);
        System.out.println(" successful SMS Trivia");
        Thread.sleep(2000);
        actions.sendKeys(Keys.ENTER);
        actions.build().perform();
        System.out.println("I am logged in successful ");

//        Actions actions=new Actions(driver);
//        Thread.sleep(2000);
//        chrome.driver.findElement(btn_create_service).click();
//        Thread.sleep(2000);
//        chrome.driver.findElement(btn_select_test).click();
//        Thread.sleep(2000);
//        chrome.driver.findElement(txt_select_type).sendKeys("SMS Trivia");
//
//        String SMS_Trivia=getText("SMS Trivia");
//
//        if (SMS_Trivia.contains("SMS Trivia")){
//            System.out.println(" successful SMS Trivia");
//        }else {
//            System.out.println(" successful SMS Trivia");
//        }
//        actions.sendKeys(Keys.ENTER);
//        actions.build().perform();
//        System.out.println("I am logged in successful ");
//    }


    }


}
