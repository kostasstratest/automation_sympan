package StepsDefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import StepsDefinitions.LoginPage;
import Pages.Browsers;
import org.openqa.selenium.chrome.ChromeDriver;

public class CreateNewService  {
  //WebDriver driver;
   WebDriver driver=null;
   Browsers chrome;
   LoginPage login;

    @Given("^that a user is on the login page$")
    public void that_a_user_is_on_the_login_page() throws InterruptedException {
      chrome = new Browsers (driver);
      chrome.chrome();



    }

    @When("^the user enters the name to the field$")
    public void the_user_enters_the_name_to_the_field() {
     login = new LoginPage();
   //login.the_user_enters_valid_credentials();
    }
    @And("^writes in the field a description$")
    public void writes_in_the_field_a_description() {

    }
    @And("^selects in the type of services the SMS trivia$")
    public void selects_in_the_type_of_services_the_sms_trivia() {

    }
    @And("^selects in the country Greece$")
    public void selects_in_the_country_greece() {

    }
    @And("^selects in the Reference time zone EuropeAthens$")
    public void selects_in_the_reference_time_zone_europe_athens() {

    }
    @And("^writes in the communication language Greek$")
    public void writes_in_the_communication_language_greek() {

    }
    @Then("^the user clicks on the save button$")
    public void the_user_clicks_on_the_save_button() {

    }


}
