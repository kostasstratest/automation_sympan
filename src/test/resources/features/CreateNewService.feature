Feature:Create new service

  Scenario: User creates a new service
    Given that a user is on the login page
    When the user enters the name to the field
    And writes in the field a description
    And selects in the type of services the SMS trivia
    And selects in the country Greece
    And selects in the Reference time zone EuropeAthens
    And writes in the communication language Greek
    Then the user clicks on the save button

