Feature:Login Page

  Scenario Outline: Scenario: User should be able to login with valid credentials
    Given the user is on login page
    When the user enters <username> and <password>
    And hits submit
    Then the user should be logged in successfully

    Examples:
      |username|password|
      |admin   |admin   |